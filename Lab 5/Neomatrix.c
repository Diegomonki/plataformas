#include <stdio.h>
#include <stdlib.h>

/*!Autores de este còdigo
// Diego "El monki" Valladares
// Tony Blandòn (Mentira, pero sí hizo el reporte)
// Jafet Soto, Soto Jafet
Prueba para generar el doxy*/
typedef
  struct _Matriz
  {
    int *ptrMatriz;
    int cantFilas;
    int cantCol;
  }
Matriz;  /*!< Se define el struct de la matriz para facilitar la creaciòn de las mismas*/

void CrearMatrizA (Matriz *mat,int f, int c){ //!<Función que me crea la matriz

      mat->cantFilas = f;  //Le aisgno un valor a cantidad de filas
      mat->cantCol = c;   //Le aisgno un valor a cantidad de columnas

      //printf("%p\n",mat->ptrMatriz);
      mat->ptrMatriz = malloc(c*f*sizeof(int)); //Asigno el espacio en la memoria para el puntero de punteros
      //printf("%p\n",mat->ptrMatriz);
      if (mat==NULL || mat->ptrMatriz==NULL){  //Salida del programa por si algo falla
          printf("Memoria insuficiente.\n");
          exit(1);
        }
      }

void CompletarMatrizA(Matriz *matr){ //!<Se completa la matriz, apoko no

  int i=0, j=0;

    for (i = 0; i < matr->cantFilas; i++) {
      for (j = 0; j < matr->cantCol; j++) {
        printf("Escriba un valor para Matriz(%d,%d) = ",i, j); //Se piden valores que seràn almacenados en las variables i y j
                                                                // El valor digitado se guardará en la posicion inicial del puntero, i será
                                                                // las filas y j las columas,ejemplo: si se digita i=2 y j=3, entonces se
                                                                // accederá a a la posición inicial*2, o sea, la segunda fila y con =3, entonces
                                                                // se accede a la cuarta posición de la fila 2.
                                                                // scanf("%d", matr->ptrMatriz+((i*matr->cantCol+j)*sizeof(int)));
        scanf( "%d", matr->ptrMatriz+((i*matr->cantCol+j)));
      }
  }
}

void ImprimirMatriz(Matriz *matriz) { //!< Función que mprime la matriz

         int i=0, j=0;
         //printf("%p\n",matriz->ptrMatriz);
         printf("La matriz es: \n");
          int r = matriz->cantFilas;
          int c = matriz->cantCol;
           for (i = 0; i < r; i++) {
             for (j = 0; j < c; j++) {
              // printf("%d\n", matriz->ptrMatriz[i * c + j]);

              printf("%d",
                // *(matriz->ptrMatriz+(i*c+j)*sizeof(int))
                *(matriz->ptrMatriz+(i*c+j)));
             printf("\t");        //tab para ordenar la matriz
             }
             printf("\n");          //cambio de linea igual para ordenar
           }
       }

void LimpiarMatriz(Matriz *mli,int f, int c){ //!< Función que limpia la matriz

  mli->cantFilas = f;  //Le aisgno un valor a cantidad de filas
  mli->cantCol = c;   //Le aisgno un valor a cantidad de columnas
  mli->ptrMatriz = (int*)calloc(c*f ,sizeof(int));
}

void BorrarMatriz(Matriz *mborr){

  free(mborr->ptrMatriz);
}

void SumarMatrices(Matriz *ms, Matriz *ma, Matriz *mb){ //!< Función que suma la matriz
  //printf("dimesiones de ms: %d, %d\n", ms->cantCol, ms->cantFilas);

  for(int i = 0; i < ms->cantFilas; i++){
    for(int j = 0; j < ms->cantCol; j++){

      int aux = *(ma->ptrMatriz+(i*ma->cantCol+j));
      int aux2 = *(mb->ptrMatriz+(i*mb->cantCol+j));
      //printf("i = %d\tj= %d \n", i, j);
      *(ms->ptrMatriz+(i*ms->cantCol+j)) = aux + aux2;
      //printf("%d = %d + %d \n", *(ms->ptrMatriz+(i*ms->cantCol+j)), aux, aux2);
    }
  }
}

void imprimir_arreglo_2D_linearizado(int* m, int r, int c){ //!< Otra funciñon que imprime cosas

    printf("%p\n", m);
    for(int i = 0; i < r; i++)
    {
        for(int j = 0; j < c; j++)
        {
            printf("%p(%d, %d): %d\n", m, i, j, *(m+(i*c+j)));
        }
    }
}

void MultiplicarMatriz(Matriz *mmul, Matriz *m1, Matriz *m2){

        for(int i = 0 ; i < mmul->cantFilas ; i++ ) //i para las filas de la matriz resultante
        {
            for(int k = 0 ; k < mmul->cantCol ; k++ ) // k para las columnas de la matriz resultante
            {
                for(int j = 0 ; j < mmul->cantCol ; j++ ) //j para realizar la multiplicacion los elementos de la matriz
                {
                  int aux = *(m1->ptrMatriz+(i*m1->cantCol+j));
                  int aux2 = *(m2->ptrMatriz+(i*m2->cantCol+k));
                    *(mmul->ptrMatriz+(j*mmul->cantCol+k)) += aux * aux2;
                }
            }
          }
        }

void MultiplicarEscalar(Matriz *mesc, int esc){

  for(int i = 0; i < mesc->cantFilas; i++){
    for(int j = 0; j < mesc->cantCol; j++){
      //printf("i = %d\tj= %d \n", i, j);
      *(mesc->ptrMatriz+(i*mesc->cantCol+j)) = (*(mesc->ptrMatriz+(i*mesc->cantCol+j)))*esc;
      //printf("%d = %d + %d \n", *(ms->ptrMatriz+(i*ms->cantCol+j)), aux, aux2);
    }
  }
}

void SumaMatricesYEScalar(Matriz *mesc, int esc){
    for(int i = 0; i < mesc->cantFilas; i++){
      for(int j = 0; j < mesc->cantCol; j++){
        //printf("i = %d\tj= %d \n", i, j);
        *(mesc->ptrMatriz+(i*mesc->cantCol+j)) = (*(mesc->ptrMatriz+(i*mesc->cantCol+j))) + esc;
        //printf("%d = %d + %d \n", *(ms->ptrMatriz+(i*ms->cantCol+j)), aux, aux2);
      }
    }
}

void TransponerMatriz(Matriz *matriz){
    int i=0,j=0;

    int r = matriz->cantFilas;
    int c = matriz->cantCol;
    printf("\nLa transpuesta es:\n ");/*IMPRIMIR MATRIZ TRANSPUESTA*/
    for (i = 0; i < c; i++) {
      for (j = 0; j < r; j++) {
       // printf("%d\n", matriz->ptrMatriz[j * c + i]);
       printf("%d",
         // *(matriz->ptrMatriz+(j*c+i)*sizeof(int))
         *(matriz->ptrMatriz+(j*r+i)));
      printf("\t");        //tab para ordenar la matriz
      }
      printf("\n");          //cambio de linea igual para ordenar
    }
}

int main(int argc, char const *argv[]) { //!< El main

    //*!se definen las matrices, se agregan dialogos para facilitar el uso del programa y tambièn se llaman las funciones
    Matriz ma;
    Matriz mb;
    Matriz ms;
    Matriz mmult;
    Matriz mlimp;
    Matriz mNueva;
    Matriz msume;
    Matriz msume2;
    Matriz mTrans;
    int escalar;

    printf("Introduzca numero de filas de la matriz A: ");
    scanf("%d", &ma.cantFilas);
    printf("Introduzca numero de columnas la matriz A: ");
    scanf("%d", &ma.cantCol);
    //printf("%p\n", (ma).ptrMatriz);
    CrearMatrizA(&ma, ma.cantFilas, ma.cantCol);
    //printf("%p\n",ma.ptrMatriz);
    CompletarMatrizA(&ma);
    //printf("%p\n",ma.ptrMatriz);
    ImprimirMatriz(&ma);

printf("--------------------------------------------\n");

    printf("Introduzca numero de filas de la matriz B: ");
    scanf("%d",&mb.cantFilas);
    printf("Introduzca numero de columnas la matriz B: ");
    scanf("%d", &mb.cantCol);
    //printf("%p\n",mb.ptrMatriz);
    CrearMatrizA(&mb, mb.cantFilas, mb.cantCol);
    //printf("%p\n",mb.ptrMatriz);
    CompletarMatrizA(&mb);
    //printf("%p\n",mb.ptrMatriz);
    ImprimirMatriz(&mb);

printf("-----------------------------------------------\n");

    CrearMatrizA(&ms, ma.cantFilas, ma.cantCol);
    printf("La suma es..\n");
    SumarMatrices(&ms, &ma, &mb);
    ImprimirMatriz(&ms);


printf("-----------------------------------------------\n");

      printf("La multiplicación es: \n");
      CrearMatrizA(&mmult, ma.cantFilas, ma.cantCol);
      MultiplicarMatriz(&mmult, &ma, &mb);
      ImprimirMatriz(&mmult);


printf("-----------------------------------------------\n");

      printf("La matriz limpia es ...\n");
      LimpiarMatriz(&mlimp, ma.cantFilas, ma.cantCol);
      ImprimirMatriz(&mlimp);

printf("-----------------------------------------------\n");

      CrearMatrizA(&msume, ma.cantFilas, ma.cantCol);
      CompletarMatrizA(&msume);
      printf("Introduzca el número que desea sumarle a cada entrada de la matriz: \n");
      scanf("%d", &escalar);
      SumaMatricesYEScalar(&msume, escalar);
      ImprimirMatriz(&msume);

printf("-----------------------------------------------\n");

      CrearMatrizA(&mNueva, ma.cantFilas, ma.cantCol);
      CompletarMatrizA(&mNueva);
      printf("Introduzca el número por el cual desea multiplicar la matriz: \n");
      scanf("%d", &escalar);
      MultiplicarEscalar(&mNueva, escalar);
      ImprimirMatriz(&mNueva);


printf("-----------------------------------------------\n");

        CrearMatrizA(&mTrans, ma.cantFilas, ma.cantCol);
        CompletarMatrizA(&mTrans);
        TransponerMatriz(&mTrans);


printf("-----------------------------------------------\n");


printf("Matrices borradas\n");
      BorrarMatriz(&ma);
      BorrarMatriz(&mb);
      BorrarMatriz(&ms);
      BorrarMatriz(&mmult);
      BorrarMatriz(&mlimp);
      BorrarMatriz(&mNueva);
      BorrarMatriz(&mTrans);

  return 0;
}
