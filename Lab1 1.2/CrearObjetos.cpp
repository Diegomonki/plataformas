#include <iostream>
#include "Pistas.h"
#include "Jugadores.h"
#include "GenerarSolucion.h"
#include "GenerarPistas.h"

using namespace std;

void CrearObjetos(){

    GenerarHabitacionSolucion();
    GenerarArmaSolucion();
    GenerarJugadorSolucion();
    GenerarPistas();

    /*Pistas*/
    Pistas Pista1 = Pistas(Carta_Aleatoria[0]);
    Pistas Pista2 = Pistas(Carta_Aleatoria[1]);
    Pistas Pista3 = Pistas(Carta_Aleatoria[2]);
    Pistas Pista4 = Pistas(Carta_Aleatoria[3]);
    Pistas Pista5 = Pistas(Carta_Aleatoria[4]);
    Pistas Pista6 = Pistas(Carta_Aleatoria[5]);
    Pistas Pista7 = Pistas(Carta_Aleatoria[6]);
    Pistas Pista8 = Pistas(Carta_Aleatoria[7]);
    Pistas Pista9 = Pistas(Carta_Aleatoria[8]);
    Pistas Pista10 = Pistas(Carta_Aleatoria[9]);
    Pistas Pista11 = Pistas(Carta_Aleatoria[10]);
    Pistas Pista12 = Pistas(Carta_Aleatoria[11]);
    Pistas Pista13 = Pistas(Carta_Aleatoria[12]);
    Pistas Pista14 = Pistas(Carta_Aleatoria[13]);
    Pistas Pista15 = Pistas(Carta_Aleatoria[14]);
    Pistas Pista16 = Pistas(Carta_Aleatoria[15]);
    Pistas Pista17 = Pistas(Carta_Aleatoria[16]);

    /*Asignación de pistas a los jugadores*/
    Jugadores Jugador1 = Jugadores("Jugador1", Pista1.Contenido, Pista2.Contenido, Pista3.Contenido);
    Jugadores Jugador2 = Jugadores("Jugador2", Pista4.Contenido, Pista5.Contenido, Pista6.Contenido);
    Jugadores Jugador3 = Jugadores("Jugador3", Pista7.Contenido, Pista8.Contenido, Pista9.Contenido);
    Jugadores Jugador4 = Jugadores("Jugador4", Pista10.Contenido, Pista11.Contenido, Pista12.Contenido);
    Jugadores Jugador5 = Jugadores("Jugador5", Pista13.Contenido, Pista14.Contenido, Pista15.Contenido);
    Jugadores Jugador6 = Jugadores("Jugador6", Pista16.Contenido, Pista17.Contenido);

}