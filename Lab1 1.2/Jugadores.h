#ifndef JUGADORES_H
#define JUGADORES_H
#include <iostream>
#include <string>

using namespace std;

class Jugadores {
    public:
    string Nombre;
    string Pista_1;
    string Pista_2;
    string Pista_3;
    bool Estado; //1 si el jugador esta activo, 0 si el jugador pierde 
    bool EnHabitacion; //Esto muestra si el jugador esta en una habitación o no

    Jugadores();
    
    Jugadores (string n , string p1, string p2, string p3) {

        Nombre = n;
        Pista_1 = p1;
        Pista_2 = p2;
        Pista_3 = p3;
        }
    
    Jugadores (string n , string p1, string p2) {  //Constructor para el jugador que solo se le asignan dos pistas

        Nombre = n;
        Pista_1 = p1;
        Pista_2 = p2;
        }

   
};


#endif
