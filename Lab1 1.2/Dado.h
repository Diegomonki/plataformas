#ifndef DADO_H
#define DADO_H
#include <iostream>
#include <random>

using namespace std;



int dado() {

  int dado1, dado2, movimientos{};

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(1, 6);
    dado1 = dis(gen);
    dado2 = dis(gen);

    movimientos += (dado1 + dado2);
    cout << "Primer dado: " << dado1 << endl;
    cout << "Segundo dado: " << dado2 << endl;
    cout << "La jugada resulta en  " << movimientos << " movimientos" << endl;
    return movimientos;
}


#endif
