#ifndef GENERAR_PISTAS_H
#define GENERAR_PISTAS_H
#include <iostream>
#include "Jugadores.h"
#include "GenerarSolucion.h"
#include "Pistas.h"

using namespace std;

string  Carta_Aleatoria[17]; // Estas son las 17 pistas disponibles para repartir luego de sacar las 3 de la solución

/*Esta funcion genera aleatoriamente las 17 pistas 
que se repartiran a los jugadores.*/
void GenerarPistas(){
int j = 1;
    for (int i = 0; i < 17; i++) {
    
    do {
        random_device rd;
        mt19937 gen(rd());
        uniform_int_distribution<> dis(0,19);

        int dis1 = dis(gen);

        if (CartaRepartida[dis1] == 0) {
        Carta_Aleatoria[i] = Cartas[dis1];
        CartaRepartida[dis1] = 1;
        
        //cout << Carta_Aleatoria[i] << endl;
        
} else (j = 0);
j++;
    }while(j == 1);
}


}
#endif
