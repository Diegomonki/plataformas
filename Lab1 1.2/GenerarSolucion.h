#ifndef GENERAR_SOLUCION_H
#define GENERAR_SOLUCION_H
#include <iostream>
#include <random>
#include "Cartas.h"

using namespace std;

string SolucionFinal[3]; //Esta variable guarda la solucion final del juego
bool CartaRepartida[20]; //Este arreglo tendrá un 1 si la carta ya fue escogida y un 0 si no. 
int random1;
int random2;
int random3; 

/*Esta funcion toma una habitacion del total y la guarda como la habitacion en la que se cometio
el asesinato*/
void GenerarHabitacionSolucion() {

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0,8);

    random1 = dis(gen);
  
    SolucionFinal[0] = Cartas[random1];
    
    CartaRepartida[random1] = 1;
    //std::cout << SolucionFinal[0] << endl;

}

/*Esta funcion toma un arma del total de armas y la guarda como el arma que fue
usada en el asesinato*/
void GenerarArmaSolucion() {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(9,13);

    random2 = dis(gen);

    SolucionFinal[1] = Cartas[random2];
    
    CartaRepartida[random2] = 1;
   // std::cout  << SolucionFinal[1] << endl;
    
}

/*Esta funcion toma un jugador del total y lo guarda como el asesino*/
void GenerarJugadorSolucion() {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(14,19);

    random3 = dis(gen);

    SolucionFinal[2] = Cartas[random3];
    CartaRepartida[random3] = 1;
  //  std::cout << SolucionFinal[2] << endl;

}
#endif
