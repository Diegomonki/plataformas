#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>

using namespace std;

//Carga 1
  double potMecanica1 = 5592.75; //en watt
  double eficienciaMedia1 = 0.865;
  double eficienciaTresCuartos1 = 0.885;
  double eficienciaPlena1 = 0.885;
  double fpMedio1 = 0.75;
  double fpTresCuartos1 = 0.84;
  double fpPleno1 = 0.89;

  //Carga 2
  double potMecanica2 = 18642.5; //en watt
  double eficienciaMedia2 = 0.914;
  double eficienciaTresCuartos2 = 0.917;
  double eficienciaPlena2 = 0.92;
  double fpMedio2 = 0.81;
  double fpTresCuartos2 = 0.86;
  double fpPleno2 = 0.88;

  //Carga 3

  double potMecanica3 = 3728.5; //en watt
  double eficienciaMedia3 = 0.87;
  double eficienciaTresCuartos3 = 0.875;
  double eficienciaPlena3 = 0.88;
  double fpMedio3 = 0.72;
  double fpTresCuartos3 = 0.82;
  double fpPleno3 = 0.85;

  //Carga 4

  double potActiva4; //en watt

  //Carga 5

  double potMecanica5 = 4101.35; //en watt
  double eficienciaPlena5 = 0.85;
  double fpPleno5 = 0.86;


double potActiva1;
double potActiva2;
double potActiva3;
double potActiva5;
double potQ1;
double potQ2;
double potQ3;
double potQ5;
double Ptotal;
double Qtotal;

MainWindow::MainWindow(QWidget *parent)    : QMainWindow(parent)    , ui(new Ui::MainWindow){
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_PushButton_clicked()
{

}

void MainWindow::on_pushButton_clicked()
{
     ui->label_2->setText("Digite la potencia 1:");

}

void MainWindow::on_pushButton_2_clicked()
{
    ui->label_3->setText("Digite la potencia 2:");
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->label_4->setText("Digite la potencia 3:");


}

void MainWindow::on_pushButton_6_clicked()
{
    potActiva1 = potMecanica1/eficienciaMedia1;

        potQ1 = potActiva1*tan(acos(fpMedio1));
}


void MainWindow::on_pushButton_9_clicked()
{
    potActiva1 = potMecanica1/eficienciaTresCuartos1;


         potQ1 = potActiva1*tan(acos(fpTresCuartos1));
}

void MainWindow::on_pushButton_14_clicked()
{
    potActiva1 = potMecanica1/eficienciaPlena1;

         potQ1 = potActiva1*tan(acos(fpPleno1));
}

void MainWindow::on_pushButton_7_clicked()
{
    potActiva2 = potMecanica2/eficienciaMedia2;

        potQ2 = potActiva2*tan(acos(fpMedio2));
}

void MainWindow::on_pushButton_12_clicked()
{
    potActiva2 = potMecanica2/eficienciaTresCuartos2;

         potQ2 = potActiva2*tan(acos(fpTresCuartos1));
}

void MainWindow::on_pushButton_10_clicked()
{
    potActiva2 = potMecanica2/eficienciaPlena2;

         potQ2 = potActiva2*tan(acos(fpPleno2));
}

void MainWindow::on_pushButton_8_clicked()
{
    potActiva3 = potMecanica3/eficienciaMedia3;

        potQ3 = potActiva3*tan(acos(fpMedio3));
}

void MainWindow::on_pushButton_13_clicked()
{
    potActiva3 = potMecanica3/eficienciaTresCuartos3;

         potQ3 = potActiva3*tan(acos(fpTresCuartos1));
}

void MainWindow::on_pushButton_11_clicked()
{
    potActiva3 = potMecanica3/eficienciaPlena3;

         potQ3 = potActiva3*tan(acos(fpPleno3));
}

void MainWindow::on_pushButton_4_clicked()
{
    potActiva4 = 15400; //en watt
}

void MainWindow::on_pushButton_5_clicked()
{
    potActiva5 = potMecanica5/eficienciaPlena5;
      potQ5 = potActiva5*tan(acos(fpPleno5));
}
double fptotal;
void MainWindow::on_pushButton_15_clicked()
{
       Ptotal = potActiva1 + potActiva2 + potActiva3 + potActiva4 + potActiva5;
       Qtotal = potQ1 + potQ2 + potQ3 + potQ5;
      double Stotal = sqrt( pow(Ptotal,2) + pow(Qtotal,2) );
      fptotal = cos((atan(Qtotal/Ptotal)));
      //double fptotalConS = Ptotal/Stotal;
      double grados = (fptotal*180)/3.1415592654;

      ui->label_5->setText("La potencia activa del sistema es de: " + QString::number(Ptotal) + " watt");
      ui->label_6->setText("La potencia reactiva del sistema es de: " + QString::number(Qtotal) + " VAr");
      ui->label_7->setText("La potencia aparente del sistema es de " + QString::number(Stotal) + " VA<" + QString::number(grados) + "°" );
      ui->label->setText("El factor de potencia del sistema es de " + QString::number(fptotal));
      ui->label_11->setText("El factor de potencia admitido por la empresa es de 0.90");
      ui->label_9->setText(" ");
      ui->label_14->setText(" ");
      if(fptotal>0.90){
          ui->label_12->setText("El factor de potencia de la industria");
          ui->label_13->setText("está dentro del rango permitido");
      }else{
          ui->label_12->setText("El factor de potencia de la industria NO");
          ui->label_13->setText("está dentro del rango permitido");
      }


}

void MainWindow::on_pushButton_16_clicked()
{
    ui->label_10->setText("Digite el nuevo factor de potencia");
}

void MainWindow::on_doubleSpinBox_valueChanged(const QString &arg1)
{
    double Qn = Ptotal*tan(acos(arg1.toDouble()));

    double Qc = (Qn - Qtotal)/1000;

    ui->label_9->setText("La potencia reactiva necesaria es de: ");
    ui->label_14->setText(QString::number(Qc) + "kVAr" );

    if(Qc<-10.0 && Qc>-15){
        ui->label_12->setText("Se debe conectar un banco de");
        ui->label_13->setText("capacitores: Varplus M 52426");
    }if(Qc<-8.5 && Qc>-10){
        ui->label_12->setText("Se debe conectar un banco de");
        ui->label_13->setText("capacitores: Varplus M 52425");
    }if(Qc<-5.0 && Qc>-8.5){
        ui->label_12->setText("Se debe conectar un banco de");
        ui->label_13->setText("capacitores: Varplus M 52424");
    }if (arg1.toDouble()<0.90) {
    ui->label_9->setText("Digite un factor de potencia");
    ui->label_14->setText("mayor a 0.90 para evitar" );
    ui->label_12->setText(" multas por demanda máxima " );
    ui->label_13->setText(" " );
    }if (arg1.toDouble()>0.90) {
        ui->label_12->setText(" " );
    }
}

void MainWindow::on_pushButton_17_clicked()
{
    potActiva1 = 0;
    potActiva2 = 0;
    potActiva3 = 0;
    potActiva4 = 0;
    potActiva5 = 0;
    potQ1 = 0;
    potQ2 = 0;
    potQ3 = 0;
    potQ5 = 0;
    ui->label->setText(" ");
    ui->label_5->setText(" ");
    ui->label_6->setText(" ");
    ui->label_7->setText(" ");
    ui->label_8->setText(" ");
    ui->label_9->setText(" ");
    ui->label_10->setText(" ");
    ui->label_11->setText(" ");
    ui->label_12->setText(" ");
    ui->label_13->setText(" ");
    ui->label_14->setText(" ");
    ui->label_8->setText(" ");
}
